<?php


/**
 * yunke_captcha模块通用管理表单（管理首页）
 */

namespace Drupal\yunke_captcha\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Cache\Cache;


class yunkeCaptchaAdminForm extends FormBase
{
    //可编辑配置对象，储存验证码设置
    protected $config = null;

    //验证码插件管理器
    protected $pluginManagerYunkeCaptcha = null;


    public function __construct($configFactory, $pluginManagerYunkeCaptcha)
    {
        $this->config = $configFactory->getEditable('yunke_captcha.settings');
        $this->pluginManagerYunkeCaptcha = $pluginManagerYunkeCaptcha;
    }

    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('config.factory'),
            $container->get('plugin.manager.yunkeCaptcha')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'yunke_captcha_admin_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['#title'] = t('验证码管理（yunke_captcha模块）');
        $form['explain'] = [
            '#type'  => 'html_tag',
            '#tag'   => 'p',
            '#value' => t('本页设置将应用到站点所有启用验证码的表单，具体某个表单的验证码管理请点击表单管理选项卡'),
        ];

        $form['manageFromForm'] = [
            '#type'          => 'checkbox',
            '#title'         => t('是否在表单中显示验证码管理链接，仅适用于有验证码管理权限的用户'),
            '#description'   => t('开启后可在目标表单中直接管理验证码相关设置，因该项开启后会为全站所有表单（包括管理表单）添加管理链接，因此强烈建议完成全站验证码配置工作后关闭该项'),
            '#default_value' => $this->config->get('manageFromForm'),
            '#field_prefix'=>'<strong>开启从表单管理</strong><br>',
        ];
        
        $form['title'] = [
            '#type'          => 'textfield',
            '#required'      => TRUE,
            '#title'         => t('验证码字段名'),
            '#default_value' => $this->config->get('title'),
            '#description'   => t('显示在验证码字段前面的文字，通常为“验证码”，在这里可自定义'),
        ];
        $form['size'] = [
            '#type'          => 'number',
            '#required'      => TRUE,
            '#title'         => t('验证码输入框尺寸'),
            '#min'           => 1,
            '#default_value' => $this->config->get('size'),
            '#description'   => t('一个整数，默认为30'),
        ];
        $form['invalid_time'] = [
            '#type'          => 'number',
            '#required'      => TRUE,
            '#title'         => t('验证码有效期'),
            '#min'           => 0,
            '#default_value' => $this->config->get('invalid_time'),
            '#description'   => t('一个整数，单位为秒，默认为10800（3小时），生成验证码后超过该时间必须重新刷新获取才能提交'),
        ];

        $captchaType = [];
        foreach ($this->pluginManagerYunkeCaptcha->getDefinitions() as $pluginID => $definitions) {
            $captchaType[$pluginID] = $definitions['label'];
        }
        $form['captchaDefaultType'] = [
            '#type'          => 'select',
            '#title'         => t('默认验证码类型'),
            '#description'   => t('为表单新建验证码时采用的默认类型，各表单可独立配置'),
            '#required'      => TRUE,
            '#options'       => $captchaType,
            '#default_value' => $this->config->get('captchaDefaultType'),
        ];


        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = array(
            '#type'        => 'submit',
            '#value'       => $this->t('Save'),
            '#button_type' => 'primary',
        );

        $declaration = \Drupal::moduleHandler()->getModule("yunke_captcha")->getPath() . '/data/declaration.txt';
        if (file_exists($declaration) && is_readable($declaration)) {
            $form['#suffix'] = new FormattableMarkup(file_get_contents($declaration), []);
        }

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array & $form, FormStateInterface $form_state)
    {
        //其他基本验证系统会自动进行
        if (empty(trim($form_state->getValue('title')))) {
            $form_state->setError($form['title'], t('请设置验证码字段名'));
        }
        $isManageFromForm = (bool)$form_state->getValue('manageFromForm');
        $form_state->setValue('manageFromForm', $isManageFromForm);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array & $form, FormStateInterface $form_state)
    {
        $form_state->cleanValues();
        $values = $form_state->getValues();
        $this->config->set('title', $values['title']);
        $this->config->set('size', $values['size']);
        $this->config->set('invalid_time', $values['invalid_time']);
        $this->config->set('captchaDefaultType', $values['captchaDefaultType']);
        $this->config->set('manageFromForm', $values['manageFromForm']);
        $this->config->save();
        Cache::invalidateTags(['yunkeCaptcha']);
        //失效系统所有表单 高负载系统可能导致缓存血崩 因此不要随便设置通用表单 
        \Drupal::messenger()->addStatus(t('保存成功'));
    }

}
