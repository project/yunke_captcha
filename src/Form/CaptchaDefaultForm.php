<?php


/**
 * 表单验证码配置实体编辑、新建表单
 */

namespace Drupal\yunke_captcha\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Utility\NestedArray;


class CaptchaDefaultForm extends EntityForm
{
    //可编辑配置对象，储存验证码设置
    protected $config = null;

    //验证码插件管理器
    protected $pluginManagerYunkeCaptcha = null;


    public function __construct($configFactory, $pluginManagerYunkeCaptcha)
    {
        $this->config = $configFactory->getEditable('yunke_captcha.settings');
        $this->pluginManagerYunkeCaptcha = $pluginManagerYunkeCaptcha;
    }

    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('config.factory'),
            $container->get('plugin.manager.yunkeCaptcha')
        );
    }


    /**
     * {@inheritdoc}
     */
    public function form(array $form, FormStateInterface $form_state)
    {
        $entity = $this->entity;
        $form['#title'] = t('表单验证码配置管理');
        $form['id'] = [
            '#type'          => 'textfield',
            '#required'      => TRUE,
            '#title'         => t('表单ID'),
            '#default_value' => $entity->id(),
            '#description'   => t('表单的form_id字段值，非新建操作不允许修改，如你不知道如何填写，可开启从表单管理验证码后，直接从表单页面添加'),
        ];
        if (!$entity->isNew()) {
            $form['id']['#disabled'] = true;
        }
        $form['label'] = [
            '#type'          => 'textfield',
            '#required'      => TRUE,
            '#title'         => t('表单名'),
            '#default_value' => $entity->label(),
            '#description'   => t('使用该表单的页面名称，帮助管理'),
        ];
        $captchaType = [];
        foreach ($this->pluginManagerYunkeCaptcha->getDefinitions() as $pluginID => $definitions) {
            $captchaType[$pluginID] = $definitions['label'];
        }
        $checker = $this->pluginManagerYunkeCaptcha->getInstance(['formID' => $entity->id()]);
        $form['captchaType'] = [
            '#type'          => 'select',
            '#title'         => t('验证码类型'),
            '#description'   => t('不同类型的验证码验证原理不同，你可以通过插件机制扩展更多类型'),
            '#required'      => TRUE,
            '#options'       => $captchaType,
            '#default_value' => $checker->getPluginId(),
            '#attributes'    => [
                'autocomplete' => 'off',
            ],
            '#ajax'          => [
                'callback' => '::checkerSettingsForm',
                'wrapper'  => 'settings-wrapper',
                'method'   => 'html',
            ],
        ];

        $form['settings'] = [
            '#type'             => false, //避免表单构建器抛出php错误，没有这一行将产生很多错误日志
            '#theme'            => 'yunke_captcha_checker_settings',
            '#tree'             => true,
            '#input'            => true,
            '#checker'          => $checker,
            '#attributes'       => [
                'id' => 'settings-wrapper',
            ],
            '#process'          => [
                [$this, 'processSettingsForm'],
            ],
            '#value_callback'   => [get_class($this), 'valueCallback'],
            '#element_validate' => [
                [$this, 'validateSettings'],
            ],
        ];

        $form['settings'] = $checker->settingsForm($form['settings'], $form_state);

        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = array(
            '#type'        => 'submit',
            '#value'       => $this->t('Save'),
            '#button_type' => 'primary',
        );

        return parent::form($form, $form_state, $entity);
    }

    /**
     * 验证控件设置
     *
     * @param array              $settings
     * @param FormStateInterface $form_state
     * @param                    $complete_form
     */
    public function validateSettings(array &$settings, FormStateInterface $form_state, &$complete_form)
    {
        $checker = $settings['#checker'];
        $checker->validateSettingsForm($settings, $form_state, $complete_form);
    }

    /**
     * AJAX回调返回的内容不会再经过表单构建器，因此设置表单将没有正确的name值，通过该方法处理，提前为回调准备好内容
     *
     * @param                    $element
     * @param FormStateInterface $form_state
     * @param                    $complete_form
     *
     * @return mixed
     */
    public function processSettingsForm(&$element, FormStateInterface $form_state, &$complete_form)
    {
        if (!$form_state->isProcessingInput()) {
            return $element;
        }
        $inputPluginId = NestedArray::getValue($form_state->getUserInput(), $complete_form['captchaType']['#parents']);
        if ($inputPluginId === $complete_form['captchaType']['#default_value']) {
            return $element;
        }
        foreach ($element as $key => $value) {
            if ($key[0] === '#') {
                continue;
            }
            unset($element[$key]);
        }
        $checker = $this->pluginManagerYunkeCaptcha->createInstance($inputPluginId);
        $element = $checker->settingsForm($element, $form_state);
        $element['#checker'] = $checker;
        return $element;
    }

    /**
     * 值回调
     *
     * @param                    $element
     * @param                    $input
     * @param FormStateInterface $form_state
     *
     * @return null
     */
    public static function valueCallback(&$element, $input, FormStateInterface $form_state)
    {
        if ($input === FALSE) {
            return NULL;
        }
        if ($form_state->isProcessingInput() && $input !== NULL) {
            return $input;
        }
        return NULL;
    }

    /**
     * ajax方式返回验证码插件设置表单
     */
    public function checkerSettingsForm($form, FormStateInterface $form_state)
    {
        $settingsForm = $form['settings'];
        foreach ($settingsForm as $key => $value) {
            if ($key[0] === '#') {
                unset($settingsForm[$key]);
            }
        }
        return $settingsForm;
    }

    /**
     * {@inheritdoc}
     */
    public function save(array $form, FormStateInterface $form_state)
    {
        $entity = $this->entity;

        $entity->set('label', trim($entity->label()));
        $status = $entity->save();

        $edit_link = $this->entity->toLink($this->t('Edit'), 'edit-form')->toString();
        if ($status == SAVED_UPDATED) {
            $this->messenger()->addStatus($this->t('表单验证码配置 %label 已被更新', ['%label' => $entity->label()]));
            $this->logger('user')->notice('表单验证码配置 %label 已被更新', ['%label' => $entity->label(), 'link' => $edit_link]);
        } else {
            $this->messenger()->addStatus($this->t('表单验证码配置 %label 已保存', ['%label' => $entity->label()]));
            $this->logger('user')->notice('表单验证码配置 %label 已保存', ['%label' => $entity->label(), 'link' => $edit_link]);
        }
        $form_state->setRedirect('entity.yunke_captcha.collection');
    }

}
