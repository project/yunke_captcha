<?php

namespace Drupal\yunke_captcha;

use Drupal\Core\Form\FormStateInterface;


/**
 * 验证器接口定义
 *
 * @TODO  将来考虑完全由验证器来处理整个验证码元素
 *
 * @ingroup yunke_captcha
 */
interface CheckerInterface
{

    /**
     * 返回本验证器的默认设置
     *
     * @return array
     */
    public static function defaultSettings();

    /**
     * 返回一个用于配置该检查器的表单
     *
     *
     * @param array                                $form
     *   The form where the settings form is being included in.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     *
     * @return array
     *   The form definition for the checker settings.
     */
    public function settingsForm(array &$form, FormStateInterface $form_state);

    /**
     * 验证设置表单
     *
     * @param array              $form
     * @param FormStateInterface $form_state
     *
     * @return mixed
     */
    public function validateSettingsForm(array &$form, FormStateInterface $form_state, array &$complete_form);

    /**
     * 得到质询问题的渲染数组
     *
     * @param $formID 表单id
     * @param $pageID 页面id
     *
     * @return array 返回质询问题渲染数组
     */
    public function getAsk($formID, $pageID);

    /**
     * 得到验证描述，用于提示用户如何输入验证码，比如“请输入图片中的字符”
     *
     * @param string $formID 表单id
     *
     * @return @return array  描述渲染数组
     */
    public function getDescription($formID = '');

    /**
     * 判断验证码是否正确，返回布尔值
     *
     * @param $formID 表单id
     * @param $pageID 页面id
     * @param $result 用户输入的验证码
     *
     * @return bool
     */
    public function check($formID, $pageID, $result);

}
