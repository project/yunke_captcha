<?php

namespace Drupal\yunke_captcha;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * 为验证器提供一个插件管理器
 *
 * @ingroup yunke_captcha
 */
class YunkeCaptchaPluginManager extends DefaultPluginManager
{

    /**
     * 配置对象，储存本模块通用配置项
     *
     * @var Drupal\Core\Config\ImmutableConfig
     */
    protected $config;


    /**
     * 构造验证器插件管理器.
     *
     * @param \Traversable               $namespaces
     * @param CacheBackendInterface      $cache_backend
     * @param ModuleHandlerInterface     $module_handler
     * @param ConfigFactoryInterface     $config_factory
     * @param EntityTypeManagerInterface $entityTypeManager
     *
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     */
    public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config_factory)
    {
        parent::__construct('Plugin/YunkeCaptcha', $namespaces, $module_handler, 'Drupal\yunke_captcha\CheckerInterface', 'Drupal\yunke_captcha\Annotation\YunkeCaptcha');

        $this->setCacheBackend($cache_backend, 'yunke_captcha_checker');
        $this->alterInfo('yunke_captcha_type');
        $this->config = $config_factory->get('yunke_captcha.settings');
    }

    /**
     * @param array $options
     *                      $options['formID']用于指定获取验证器的表单id
     *
     * @return false|object
     * @throws \Drupal\Component\Plugin\Exception\PluginException
     */
    public function getInstance(array $options = [])
    {
        $captchaDefaultType = $this->config->get('captchaDefaultType');
        if (empty($options['formID'])) {
            return $this->createInstance($captchaDefaultType, []);
        }
        $captchaEntity = \Drupal::entityQuery('yunke_captcha')
            ->condition('id', $options['formID'])
            ->condition('status', true)
            ->execute();
        if (empty($captchaEntity)) {
            return $this->createInstance($captchaDefaultType, []);
        }
        $captchaEntity = \Drupal::entityTypeManager()->getStorage('yunke_captcha')->load($options['formID']);
        return $this->createInstance($captchaEntity->getCaptchaType(), $captchaEntity->getCheckerSettings());
    }
}