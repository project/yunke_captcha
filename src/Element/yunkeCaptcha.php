<?php

namespace Drupal\yunke_captcha\Element;

use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Component\Utility\NestedArray;

/**
 * 为表单提供验证码
 *
 * @FormElement("yunke_captcha")
 */
class yunkeCaptcha extends FormElement
{
    /**
     * {@inheritdoc}
     */
    public function getInfo()
    {
        $class = get_class($this); //不要用$this直接调用，因为信息数组会被缓存，引起缓存不更新问题
        return [
            '#input'            => TRUE,
            '#required'         => TRUE,
            '#process'          => [
                [$class, 'processYunkeCaptcha'],
                [$class, 'processGroup'],
            ],
            '#element_validate' => [
                [$class, 'validateYunkeCaptcha'],
            ],
            '#pre_render'       => [
                [$class, 'preRenderGroup'],
            ],
            '#theme'            => 'yunke_captcha_captcha',
            '#theme_wrappers'   => ['form_element'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function valueCallback(&$element, $input, FormStateInterface $form_state)
    {
        if ($input === false) {
            return NULL;
        }
        return true;
    }


    /**
     * 添加表单验证码
     *
     * @param                    $element
     * @param FormStateInterface $form_state
     * @param                    $complete_form
     *
     * @return mixed
     */
    public static function processYunkeCaptcha(&$element, FormStateInterface $form_state, &$complete_form)
    {
        $formId = $form_state->getFormObject()->getFormId();

        if (empty($element['#title'])) {
            $element['#title'] = t(\Drupal::config('yunke_captcha.settings')->get('title') ?: '验证码');
        }
        if (empty($element['#size'])) {
            $element['#size'] = (int)\Drupal::config('yunke_captcha.settings')->get('size') ?: 30;
        }
        $element['#formID'] = $formId;
        $element['#formState'] = $form_state; //提供给修改钩子使用
        $pageID = \Drupal::service("uuid")->generate();
        $checker = \Drupal::service('plugin.manager.yunkeCaptcha')->getInstance(['formID' => $formId]);
        $element['#checker'] = $checker;

        
        if (isset($element['#attributes']['class'])) {
            $element['#attributes']['class'] = array_merge($element['#attributes']['class'], ['yunke_captcha', 'yunke_captcha_' . $formId]);
        } else {
            $element['#attributes']['class'] = ['yunke_captcha', 'yunke_captcha_' . $formId];
        }

        //@todo 未来版本考虑完全由验证器处理验证码元素 以获取更多灵活性
        $element['yunkeCaptchaAsk'] = [
            'ask'         => $checker->getAsk($formId, $pageID),//检查器的该方法返回渲染数组，因此也可以返回自定义资源库
            '#theme'      => 'yunke_captcha_ask',
            '#attributes' => [
                'class' => ['yunke_captcha_ask', 'yunke_captcha_ask_' . $formId],
            ],
        ];
        
        $element['yunkeCaptchaInput'] = [
            'yunkeCaptcha'       => [
                '#type'           => 'textfield',
                '#size'           => $element['#size'],
                '#required'       => TRUE,
                '#attributes'     => ['autocomplete' => 'off'],
                '#theme_wrappers' => NULL,
            ],
            'yunkeCaptchaPageID' => [
                '#type'  => 'hidden',
                '#value' => $pageID,
            ],
            '#theme'             => 'yunke_captcha_input',
            '#attributes'        => [
                'class' => ['yunke_captcha_input', 'yunke_captcha_input_' . $formId],
            ],
        ];

        $element['yunkeCaptchaDescription'] = [
            'description' => $checker->getDescription($formId),
            '#theme'      => 'yunke_captcha_description',
            '#attributes' => [
                'class' => ['yunke_captcha_description', 'yunke_captcha_description_' . $formId],
            ],
        ];

        \Drupal::moduleHandler()->alter('yunke_captcha_element', $element, $formId);
        return $element;
    }


    /**
     * 执行验证码验证
     *
     * @param                    $element
     * @param FormStateInterface $form_state
     * @param                    $complete_form
     */
    public static function validateYunkeCaptcha(&$element, FormStateInterface $form_state, &$complete_form)
    {
        $input = NestedArray::getValue(
            $form_state->getUserInput(),
            $element['yunkeCaptchaInput']['yunkeCaptcha']['#parents']
        );

        $pageID = NestedArray::getValue(
            $form_state->getUserInput(),
            $element['yunkeCaptchaInput']['yunkeCaptchaPageID']['#parents']
        );
        $form_state->setValue($element['#parents'], ['captcha' => $input, 'pageID' => $pageID]);
        $checker = $element['#checker'];
        $formId = $element['#formID'];
        $result = $checker->check($formId, $pageID, $input);
        if (!$result) {
            $form_state->setError($element, $element['#title'] . t('不正确'));
        }
    }


}
