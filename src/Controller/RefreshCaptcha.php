<?php
/**
 *  by:yunke
 *  email:phpworld@qq.com
 *  time:20191026
 */

namespace Drupal\yunke_captcha\Controller;

use Symfony\Component\HttpFoundation\Response;

class RefreshCaptcha
{
    /**
     * 通过AJAX得到刷新后的验证码质询问题
     */
    public function getAsk($formId, $pageId)
    {
        \Drupal::service('page_cache_kill_switch')->trigger();
        $checker = \Drupal::service('plugin.manager.yunkeCaptcha')->getInstance(['formID' => $formId]);
        $ask = $checker->getAsk($formId, $pageId, false);
        $ask = \Drupal::service('renderer')->renderRoot($ask);
        $response = new Response($ask);
        return $response;
    }

}

