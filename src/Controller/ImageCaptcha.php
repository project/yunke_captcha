<?php
/**
 *  by:yunke
 *  email:phpworld@qq.com
 *  time:20191026
 */

namespace Drupal\yunke_captcha\Controller;

use Drupal\yunke_captcha\ImageCaptchaResponse;

class ImageCaptcha
{
    /**
     * 通过AJAX得到刷新后的验证码元素后，由该控制器输出图片二进制内容
     */
    public function getImage($formId, $pageId)
    {
        $checker = \Drupal::service('plugin.manager.yunkeCaptcha')->getInstance(['formID' => $formId]);
        $imageCaptchaGenerator = $checker->getImageCaptcha($formId, $pageId);
        return new ImageCaptchaResponse($imageCaptchaGenerator);
    }

}

