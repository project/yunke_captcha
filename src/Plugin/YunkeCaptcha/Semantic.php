<?php

namespace Drupal\yunke_captcha\Plugin\YunkeCaptcha;

use Drupal\yunke_captcha\CheckerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Url;


/**
 * 定义一个语义问答类型的验证器，如“十里长街送总理中总理的名字？",答“周恩来”
 * 需提供语义库，将从中随机选择
 *
 * @YunkeCaptcha(
 *   id = "semantic",
 *   label = @Translation("语义问答"),
 *   description = @Translation("随机从语义问答库提取问题进行验证，需预先设置语义问答库"),
 * )
 */
class Semantic extends PluginBase implements CheckerInterface, ContainerFactoryPluginInterface
{

    //设置数组
    protected $settings = NULL;

    //配置工厂 $config_factory->get('yunke_captcha.settings');
    protected $configFactory;

    //一个数组表示的语义列表文件
    protected $semanticList = NULL;

    public function __construct($configuration, $plugin_id, $plugin_definition, $config_factory)
    {
        parent::__construct($configuration, $plugin_id, $plugin_definition);
        $this->settings = array_intersect_key($configuration, self::defaultSettings()) + self::defaultSettings();
        $this->configFactory = $config_factory;
    }

    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get('config.factory')
        );
    }

    public static function defaultSettings()
    {
        return [
            'isCaseSensitive' => FALSE, //是否大小写敏感，默认大小写均可 
        ];
    }

    public function settingsForm(array &$form = [], FormStateInterface $form_state)
    {
        $form['isCaseSensitive'] = [
            '#type'          => 'checkbox',
            '#title'         => t('输入是否区分大小写'),
            '#default_value' => $this->settings['isCaseSensitive'],
            '#attributes'    => [
                'autocomplete' => 'off',
            ],
        ];
        return $form;
    }

    public function validateSettingsForm(array &$form, FormStateInterface $form_state, array &$complete_form)
    {
        $isCaseSensitive = (bool)$form_state->getValue($form['isCaseSensitive']['#parents']);
        $form_state->setValue($form['isCaseSensitive']['#parents'], $isCaseSensitive);
    }


    public function getDescription($formID = '')
    {
        $msg = '';
        if ($this->settings['isCaseSensitive']) {
            $msg .= t('区分大小写');
        } else {
            $msg .= t('不区分大小写');
        }
        return [
            '#markup' => t('请输入以上问题的答案，换一个问题请刷新，') . $msg,
        ];

    }


    public function getAsk($formID, $pageID, $isComplete = true)
    {
        if ($isComplete) {
            $url = Url::fromRoute('yunke_captcha.refreshCaptcha', ['formId' => $formID, 'pageId' => $pageID])
                ->toString(FALSE);
            $ask = [
                'content' => [
                    '#markup' => '<span class="yunke_captcha_ask_content yunke_captcha_ask_content_' . $formID . '"></span>',
                ],
                'refresh' => [
                    '#markup' => ' <a href="' . $url . '" class="yunke_captcha_refresh yunke_captcha_refresh_' . $formID . '">' . t('刷新') . '</a>',
                ],
            ];
            $ask['#attached']['library'] = ['yunke_captcha/captcha'];
            return $ask;
        }

        $request = \Drupal::request();
        $session = $request->getSession();
        // 验证码ID储存公用数据结构如下：
        // $captcha[$formID][$pageID]=['captchaID'=>$captchaID,'createdTime'=>time()];
        // $session->set('yunkeCaptcha', $captcha);
        $captcha = $session->get('yunkeCaptcha', NULL);
        if (isset($captcha[$formID][$pageID]['captchaID'])) {
            $preID = $captcha[$formID][$pageID]['captchaID'];
        } else {
            $preID = NULL;
        }

        $semanticList = $this->getSemanticList();
        $maxKey = count($semanticList) - 1;
        if ($maxKey > 0) {
            if (function_exists('random_int')) {
                $randomFun = 'random_int';
            } else {
                $randomFun = 'mt_rand';
            }
            while (TRUE) {
                $id = $randomFun(0, $maxKey);
                if ($id !== $preID) {
                    break;
                }
            }
        } else {
            $id = $maxKey;
        }

        $ask = [
            '#markup' => $semanticList[$id][0],
            '#cache'  => ['max-age' => 0],
        ];
        $captcha[$formID][$pageID] = ['captchaID' => $id, 'createdTime' => time()];
        $session->set('yunkeCaptcha', $captcha);
        return $ask;
    }

    protected function getSemanticList()
    {
        if (isset($this->semanticList)) {
            return $this->semanticList;
        }
        $semanticListFile = \Drupal::moduleHandler()
                ->getModule("yunke_captcha")
                ->getPath() . '/data/semanticList.php';
        include($semanticListFile);
        $this->semanticList = $semanticList;
        return $this->semanticList;
    }


    public function check($formID, $pageID, $result)
    {
        $request = \Drupal::request();
        $session = $request->getSession();
        if (!$session) {
            return FALSE;
        }
        $captcha = $session->get('yunkeCaptcha', NULL);
        if (!isset($captcha[$formID][$pageID]['captchaID'])) {
            return false;
        }
        $id = $captcha[$formID][$pageID]['captchaID'];
        $createdTime = $captcha[$formID][$pageID]['createdTime'];
        $yunkeCaptchaSettings = $this->configFactory->get('yunke_captcha.settings');
        $exceedTime = (int)$yunkeCaptchaSettings->get('invalid_time');
        unset($captcha[$formID][$pageID]);
        $session->set('yunkeCaptcha', $captcha);

        if ((time() - $createdTime) > $exceedTime) {
            return false;
        }

        $id = (int)$id;
        $key = $this->getSemanticList()[$id][1];
        if (!$this->settings['isCaseSensitive']) {
            $key = mb_strtolower($key);
            $result = mb_strtolower($result);
        }
        if ($result === $key) {
            return TRUE;
        }
        return FALSE;
    }

}
