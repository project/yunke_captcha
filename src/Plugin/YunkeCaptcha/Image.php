<?php

namespace Drupal\yunke_captcha\Plugin\YunkeCaptcha;

use Drupal\yunke_captcha\ImageCheckerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Url;
use Drupal\yunke_captcha\Component\ImageCaptcha\ImageCaptchaGenerator;


/**
 * 定义传统类型的图片验证码，支持中文汉字、字母、数字或混合类型
 *
 * @YunkeCaptcha(
 *   id = "image",
 *   label = @Translation("图片"),
 *   description = @Translation("验证码用有干扰像素的图片显示，支持中文、字母、数字"),
 * )
 */
class Image extends PluginBase implements ImageCheckerInterface, ContainerFactoryPluginInterface
{

    //设置数组
    protected $settings = NULL;

    //配置工厂 取得配置数组$configFactory->get('yunke_captcha.settings');
    protected $configFactory;

    //一个数组表示的中文汉字
    protected $cnChrList = NULL;

    public function __construct($configuration, $plugin_id, $plugin_definition, $config_factory)
    {
        parent::__construct($configuration, $plugin_id, $plugin_definition);
        $this->settings = array_intersect_key($configuration, self::defaultSettings()) + self::defaultSettings();
        $this->configFactory = $config_factory;
    }

    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get('config.factory')
        );
    }

    public static function defaultSettings()
    {
        return [
            'type'            => 'cn', //常用中文汉字cn、数字number、字母letter、三种类型混合mix
            'isCaseSensitive' => FALSE, //是否大小写敏感，默认大小写均可 
            'number'          => 4, //验证码字符数
            'width'           => 200,
            'height'          => 80,
            'pixelDensity'    => 0.05,
            'isSand'          => false,
            'numLine'         => NULL,
            'numArc'          => null,
            'imageType'       => 'png',
        ];
    }

    public function settingsForm(array &$form = [], FormStateInterface $form_state)
    {
        $typeOptions = [
            'cn'     => t('中文汉字'),
            'number' => t('数字'),
            'letter' => t('大小写字母'),
            'mix'    => t('混合')
        ];
        $form['type'] = [
            '#type'          => 'select',
            '#title'         => t('验证码字符类型'),
            '#required'      => TRUE,
            '#options'       => $typeOptions,
            '#default_value' => $this->settings['type'],
        ];
        $form['isCaseSensitive'] = [
            '#type'          => 'checkbox',
            '#title'         => t('字母是否区分大小写'),
            '#default_value' => $this->settings['isCaseSensitive'],
            '#attributes'    => [
                'autocomplete' => 'off',
            ],
        ];
        $form['number'] = [
            '#type'          => 'number',
            '#title'         => t('验证码字符个数'),
            '#description'   => t('至少一个'),
            '#required'      => TRUE,
            '#min'           => 1,
            '#default_value' => $this->settings['number'],
        ];
        $form['width'] = [
            '#type'          => 'number',
            '#title'         => t('验证码图片宽度'),
            '#required'      => TRUE,
            '#description'   => t('以像素为单位的一个整数'),
            '#min'           => 20,
            '#default_value' => $this->settings['width'],
        ];
        $form['height'] = [
            '#type'          => 'number',
            '#title'         => t('验证码图片高度'),
            '#required'      => TRUE,
            '#description'   => t('以像素为单位的一个整数'),
            '#min'           => 20,
            '#default_value' => $this->settings['height'],
        ];
        $form['pixelDensity'] = [
            '#type'          => 'number',
            '#title'         => t('干扰像素浓度'),
            '#required'      => TRUE,
            '#description'   => t('干扰像素和图片总像素的比值，0到1之间，0将不添加'),
            '#max'           => 1,
            '#min'           => 0,
            '#step'          => 0.01,
            '#default_value' => $this->settings['pixelDensity'],
        ];
        $form['isSand'] = [
            '#type'          => 'checkbox',
            '#title'         => t('验证码字符是否允许沙粒化'),
            '#description'   => t('随机开启像沙粒落地一样显示,和干扰像素融合在一起，破解识别难度更大'),
            '#default_value' => $this->settings['isSand'],
            '#attributes'    => [
                'autocomplete' => 'off',
            ],
        ];
        $form['numLine'] = [
            '#type'          => 'number',
            '#title'         => t('干扰直线条数'),
            '#description'   => t('0将不添加，留空将依据图片大小自动产生'),
            '#min'           => 0,
            '#default_value' => $this->settings['numLine'],
        ];
        $form['numArc'] = [
            '#type'          => 'number',
            '#title'         => t('干扰弧线条数'),
            '#description'   => t('0将不添加，留空将依据图片大小自动产生'),
            '#min'           => 0,
            '#default_value' => $this->settings['numArc'],
        ];
        $imageType = [
            'png'  => 'png',
            'jpeg' => 'jpeg',
            'gif'  => 'gif',
        ];
        $form['imageType'] = [
            '#type'          => 'select',
            '#title'         => t('图像格式'),
            '#required'      => TRUE,
            '#options'       => $imageType,
            '#description'   => t('验证码图像格式（MimeType）'),
            '#default_value' => $this->settings['imageType'],
        ];
        return $form;
    }

    public function validateSettingsForm(array &$form, FormStateInterface $form_state, array &$complete_form)
    {
        $isCaseSensitive = (bool)$form_state->getValue($form['isCaseSensitive']['#parents']);
        $form_state->setValue($form['isCaseSensitive']['#parents'], $isCaseSensitive);

        $isSand = (bool)$form_state->getValue($form['isSand']['#parents']);
        $form_state->setValue($form['isSand']['#parents'], $isSand);

        $numLine = $form_state->getValue($form['numLine']['#parents']);
        if (!is_numeric($numLine)) {
            $form_state->setValue($form['numLine']['#parents'], NULL);
        }

        $numArc = $form_state->getValue($form['numArc']['#parents']);
        if (!is_numeric($numArc)) {
            $form_state->setValue($form['numArc']['#parents'], NULL);
        }
    }


    public function getDescription($formID = '')
    {
        $msg = '';
        if ($this->settings['isCaseSensitive']) {
            $msg .= t('区分大小写');
        } else {
            $msg .= t('不区分大小写');
        }
        return [
            '#markup' => t('请输入图片中的字符，') . $msg,
        ];

    }


    public function getAsk($formID, $pageID, $isComplete = true)
    {
        if ($isComplete) {
            $url = Url::fromRoute('yunke_captcha.refreshCaptcha', ['formId' => $formID, 'pageId' => $pageID])
                ->toString(FALSE);
            $ask = [
                'content' => [
                    '#type'       => 'html_tag',
                    '#tag'        => 'div',
                    '#attributes' => [
                        'class' => ['yunke_captcha_ask_content', 'yunke_captcha_ask_content_' . $formID],
                        'style' => "width:{$this->settings['width']}px; height:{$this->settings['height']}px;",
                    ],
                ],
                'refresh' => [
                    '#type'       => 'html_tag',
                    '#tag'        => 'a',
                    '#attributes' => [
                        'class' => ['yunke_captcha_refresh', 'yunke_captcha_refresh_' . $formID],
                        'href'  => $url,
                    ],
                    '#value'      => t('刷新'),
                ],
            ];
            $ask['#attached']['library'] = ['yunke_captcha/captcha'];
            return $ask;
        }
        $request = \Drupal::request();
        $session = $request->getSession();
        // 验证码ID储存公用数据结构如下：
        // $captcha[$formID][$pageID]=['captchaID'=>$captchaID,'createdTime'=>time()];
        // $session->set('yunkeCaptcha', $captcha);
        // 本验证器的验证码ID即是验证码
        $captcha = $session->get('yunkeCaptcha', NULL);
        $preCaptcha = NULL;
        if (isset($captcha[$formID][$pageID]['captchaID'])) {
            $preCaptcha = $captcha[$formID][$pageID]['captchaID'];
        }
        $type = $this->settings['type'];
        $num = $this->settings['number'];
        while (TRUE) {
            $newCaptcha = $this->getRandomString($type, $num);
            if ($newCaptcha !== $preCaptcha) {
                break;
            }
        }
        $captcha[$formID][$pageID] = ['captchaID' => $newCaptcha, 'createdTime' => time()];
        $session->set('yunkeCaptcha', $captcha);

        //该处输出一个图片HTML元素 图片二进制内容由专用路由负责输出
        $url = Url::fromRoute('yunke_captcha.imageCaptcha',
            ['formId' => $formID, 'pageId' => $pageID],
            ['query' => ['key' => $this->random(10000, 99999)]])
            ->toString(FALSE);
        $ask = [
            '#type'       => 'html_tag',
            '#tag'        => 'img',
            '#attributes' => ['src' => $url],
            '#cache'      => ['max-age' => 0],
        ];
        return $ask;
    }

    public function getImageCaptcha($formID, $pageID)
    {
        $option = ['width', 'height', 'pixelDensity', 'isSand', 'numLine', 'numArc', 'imageType'];
        $option = array_intersect_key($this->settings, array_flip($option));
        $request = \Drupal::request();
        if ($request->hasSession()) {
            $session = $request->getSession();
        } else {
            $option['str'] = t('验证码丢失');
            return new ImageCaptchaGenerator($option);
        }
        $captcha = $session->get('yunkeCaptcha', NULL);
        if (isset($captcha[$formID][$pageID]['captchaID'])) {
            $option['str'] = $captcha[$formID][$pageID]['captchaID'];
        } else {
            $option['str'] = t('验证码丢失');
        }
        return new ImageCaptchaGenerator($option);
    }

    /**
     * 得到一个随机字符串
     *
     * @param string $type 字符类型：中文汉字cn、数字number、字母letter、三种类型混合mix
     * @param int    $num  字符串中的字符个数
     *
     * @return string
     */
    protected function getRandomString($type = 'letter', $num = 4)
    {
        if ($num <= 0) {
            $num = 1;
        }
        $type = strtolower($type);
        if (!in_array($type, ['cn', 'number', 'letter', 'mix'])) {
            $type = 'letter';
        }
        $str = '';
        if ($type === 'mix') {
            for ($i = 0; $i < $num; $i++) {
                $getMethod = ['getRandomLetter', 'getRandomNumber', 'getRandomCn'][$this->random(0, 2)];
                $str .= $this->$getMethod();
            }
        } else {
            for ($i = 0; $i < $num; $i++) {
                $getMethod = ['letter' => 'getRandomLetter', 'number' => 'getRandomNumber', 'cn' => 'getRandomCn'][$type];
                $str .= $this->$getMethod();
            }
        }
        return $str;
    }

    /**
     * 随机得到一个字母
     *
     * @return string
     */
    protected function getRandomLetter()
    {
        $str = 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
        //将iIlOo去掉（容易和1、0混淆）,剩下47个
        $index = $this->random(0, 46);
        return $str[$index];
    }

    /**
     * 随机得到一个数字
     *
     * @return string
     */
    protected function getRandomNumber()
    {
        if ($this->settings['type'] === 'number') {
            return $this->random(0, 9);
        }
        //去掉0，任意和Oo混淆
        return $this->random(1, 9);
    }

    /**
     * 随机得到单个中文字符
     *
     * @return string
     */
    protected function getRandomCn()
    {
        $cnChrList = $this->getCnChrList();
        $max = count($cnChrList) - 1;
        if ($max > 0) {
            $index = $this->random(0, $max);
        } else {
            $index = 0;
        }
        return $cnChrList[$index];
    }

    /**
     * 得到中文字符列表数组
     *
     * @return array[] string[]
     */
    protected function getCnChrList()
    {
        if (isset($this->cnChrList)) {
            return $this->cnChrList;
        }
        $cnChrListFile = \Drupal::moduleHandler()
                ->getModule("yunke_captcha")
                ->getPath() . '/data/cnChrList.php';
        include($cnChrListFile);
        $this->cnChrList = preg_split('/(?<!^)(?!$)/u', $cnChrList);
        return $this->cnChrList;
    }

    /**
     * 得到随机整数
     *
     * @return int
     */
    protected function random($min = 0, $max = NULL)
    {
        static $randomFun = NULL;
        if ($randomFun) {
            return $randomFun($min, $max);
        }
        if (function_exists('random_int')) {
            $randomFun = 'random_int';
        } else {
            $randomFun = 'mt_rand';
        }
        return $randomFun($min, $max);
    }


    public function check($formID, $pageID, $result)
    {
        $request = \Drupal::request();
        $session = $request->getSession();
        if (!$session) {
            return FALSE;
        }
        $captcha = $session->get('yunkeCaptcha', NULL);
        if (!isset($captcha[$formID][$pageID]['captchaID'])) {
            return false;
        }
        $key = $captcha[$formID][$pageID]['captchaID'];
        $createdTime = $captcha[$formID][$pageID]['createdTime'];
        $yunkeCaptchaSettings = $this->configFactory->get('yunke_captcha.settings');
        $exceedTime = (int)$yunkeCaptchaSettings->get('invalid_time');
        unset($captcha[$formID][$pageID]);
        $session->set('yunkeCaptcha', $captcha);

        if ((time() - $createdTime) > $exceedTime) {
            return false;
        }

        if (!$this->settings['isCaseSensitive']) {
            $key = mb_strtolower($key);
            $result = mb_strtolower($result);
        }
        if ($result === $key) {
            return TRUE;
        }
        return FALSE;
    }

}
